import React, { useState, useEffect } from "react";
import Header from "./components/Header";
import Main from "./components/Main";
import Footer from "./components/Footer";
import "./scss/App.scss";

function App() {
  const [cartItems, setCartItems] = useState([]);
  const [favorites, setFavorites] = useState([]);

  useEffect(() => {
    const savedCartItems = JSON.parse(localStorage.getItem("cartItems")) || [];
    const savedFavorites = JSON.parse(localStorage.getItem("favorites")) || [];

    setCartItems(savedCartItems);
    setFavorites(savedFavorites);
  }, [cartItems, favorites]);

  return (
    <>
      <Header
        cartItemsCount={cartItems.length}
        favoritesCount={favorites.length}
      />
      <Main cartItems={cartItems} favorites={favorites} />
      <Footer />
    </>
  );
}

export default App;