import React from 'react'
import '../scss/Button.scss'

function Button({backgroundColor, text, onClick}) {
  return (
    <button className="open-modal-button" style={{ backgroundColor }} onClick={onClick}>{text}</button>
  )
}

export default Button