import React from 'react';
import { FaShoppingCart, FaStar } from "react-icons/fa";
import '../scss/App.scss';
import '../scss/Header.scss';

const Header = ({ cartItemsCount, favoritesCount }) => {
  return (
    <header className="header">
      <div className="container">
        <div className="header__block">
          <div>LOGO</div>
          <div className="header__icons">
            <div>
              <FaShoppingCart />
              <span>{cartItemsCount}</span>
            </div>
            <div>
              <FaStar />
              <span>{favoritesCount}</span>
            </div>
          </div>
        </div>
      </div>
    </header>
  );
};
export default Header;