import React from 'react'

import '../scss/Main.scss'
import Products from './Products'

const Main = ({ cartItems, favorites }) => {
  return (
    <div className="main">
      <Products cartItems={cartItems} favorites={favorites} />
    </div>
  );
};

export default Main