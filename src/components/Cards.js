import React, { useState, useEffect } from "react";
import { FaStar } from "react-icons/fa";
import Modal from "./Modal";
import Button from "./Button";
import PropTypes from "prop-types";
import '../scss/Cards.scss'

const Cards = ({ products}) => {
  const [showModal, setShowModal] = useState(false);
  const [selectedProduct, setSelectedProduct] = useState(null);
  const [cartItems, setCartItems] = useState([]);
  const [favorites, setFavorites] = useState([]);

  
  useEffect(() => {
    const savedCartItems = JSON.parse(localStorage.getItem("cartItems")) || [];
    const savedFavorites = JSON.parse(localStorage.getItem("favorites")) || [];

    setCartItems(savedCartItems);
    setFavorites(savedFavorites);
    setSelectedProduct(null);
  }, []);

  const handleAddToCart = (product) => {
    setSelectedProduct(product);
    setShowModal(true);
  };

  const handleAddToCartConfirmation = () => {
    const updatedCartItems = [...cartItems, selectedProduct];
    localStorage.setItem("cartItems", JSON.stringify(updatedCartItems));
    setCartItems(updatedCartItems);
    setShowModal(false);
  };

  const handleAddToFavorites = (product) => {
    const updatedFavorites = favorites.includes(product.id)
      ? favorites.filter((fav) => fav !== product.id)
      : [...favorites, product.id];

    setFavorites(updatedFavorites);
    localStorage.setItem("favorites", JSON.stringify(updatedFavorites));
  };

  const handleCloseModal = () => {
    setShowModal(false);
  };

  const handleOkButtonClick = () => {
    handleAddToCartConfirmation();
  };

  const modalActions = (
    <>
      <Button backgroundColor="black" text="Ok" onClick={handleOkButtonClick} />
      <Button
        backgroundColor="black"
        text="Cancel"
        onClick={handleCloseModal}
      />
    </>
  );

  return (
    <section className="cards-products">
      <div className="container">
        <h2 className="cards-products__title">Виберіть ноутбук</h2>
        <div className="cards-products__cards">
          {products &&
            products.map((product) => (
              <article
                key={product.id}
                className="cards-products__card card-product"
              >
                <div className="card-product__image-box">
                  <img src={product.url} alt={product.name} />
                </div>
                <div className="card-product__details">
                  <h4 className="card-product__name">{product.name}</h4>
                  <p className="card-product__color">{product.color}</p>
                  <div className="card-product__buy-block">
                    <p className="card-product__price">{product.price}$</p>
                    <FaStar
                      className={`card-product__favorite-icon ${
                        favorites.includes(product.id) ? "selected" : ""
                      }`}
                      onClick={() => handleAddToFavorites(product)}
                    />
                    <button
                      className="card-product__add-button"
                      onClick={() => handleAddToCart(product)}
                    >
                      Add to cart
                    </button>
                  </div>
                </div>
              </article>
            ))}
        </div>
      </div>
      {selectedProduct && showModal && (
        <Modal
          header="Товар додано в кошик!"
          closeButton={true}
          text=""
          actions={modalActions}
          onClose={handleCloseModal}
        />
      )}
    </section>
  );
};

Cards.propTypes = {
  products: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired,
      color: PropTypes.string.isRequired,
      price: PropTypes.number.isRequired,
      url: PropTypes.string.isRequired,
    })
  ).isRequired,
};

export default Cards;
